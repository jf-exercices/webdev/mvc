<?php

namespace app\models;

use app\helpers\database;

class user
{
    public function getAll(): array
    {
        $courses = [];
        $connect = database::connect();
        $request = $connect->query("SELECT * FROM user");
        while ($course = $request->fetchObject()) {
            $courses[] = $course;
        }
        return $courses;
    }
}