<?php

namespace app\models;

use app\helpers\database;

class course
{
    public function getAll(): array
    {
        $courses = [];
        $connect = database::connect();
        $request = $connect->query("SELECT * FROM course");
        while ($course = $request->fetchObject()) {
            $courses[] = $course;
        }
        return $courses;
    }
}