<?php

namespace app\controllers;

use app\models\user;
use app\helpers\output;

class users
{
    public function list(): void
    {
        $model = new user();

        // output generated in helpers with VIEWS directory
        $data = new \stdClass();
        $data->data = $model->getAll();
        output::getContent('users/userslist', $data);
    }
}