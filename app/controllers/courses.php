<?php

namespace app\controllers;

use app\helpers\output;
use app\models\course;

class courses
{
    public function list(): void
    {
        $model = new course();

        // output generated ONLY in helpers
//        output::courseslist($model->getAll(), ['id', 'name', 'code']);

        // output generated in helpers with VIEWS directory
        $data = new \stdClass();
        $data->data = $model->getAll();
        output::getContent('courses/courseslist', $data);
    }
}